package CasePackage;
import java.io.Serializable;

import CyclistePackage.Cycliste;
import CyclistePackage.Sprinteur;

/**
 * cette classe permet de creer une case
 *  
 * @author Tristan 
 * @version 1.0
 * 
 */

public class Case implements Serializable{
		
	

	/** 
	 * attribut qui permet de savoir de quel type est la case
	 * 	0 pour ligne droite 
	 * 	1 pour virage
	 * 	2 pour départ
	 * 	3 pour arrivée
	 * 
	 */
	public int type;
	
	
	/** attribut Cycliste qui correspond au cycliste present sur la case
	 * 
	 */
	private Cycliste cycliste;
	
	
	/** attribut qui correspond au nom de la case en fonction de son type
	 * 
	 */
	private String nom;
	
	
	/** attribut qui permet de savoir si le terrain et en montee ou en descente
	 * 2 pour les montees
	 * 1 pour les descentes
	 * et 0 pour les terrains plat
	 * 
	 */
	public int relief;
	
	
	/** attribut qui permet de savoir de quel cote est la case
	 * 		true pour droite 
	 * 		et false pour gauche
	 * 
	 */
	public boolean droite;
	
	
	/** constructeur de la classe case
	 * permet de construire une case en fonction de son type
	 * 
	 * @param t type de la case
	 * @param r relief de la case 
	 * @param b boolean droite
	 * 
	 */
	public Case(int t,int r,boolean b) {
		this.type=t;
		this.relief=r;
		this.droite=b;
		switch (t) {
			case 0: 
				if(this.relief==2) {
					this.nom="Ligne droite montee";
				}else if(this.relief==1){
					this.nom="Ligne droite descente";
				}else if(this.relief==0){
					this.nom="Ligne droite plate";
				}
			break;
			case 1:
				if(this.relief==2) {
					this.nom="virage montee";
				}else if(this.relief==1){
					this.nom="virage descente";
				}else if(this.relief==0){
					this.nom="virage plat";
				}
			break;
		}
	}
	
	
	/**methode qui retourne le nom de la case
	 * 
	 * @return nom de la case
	 */
	public String getNom() {
		return nom;
	}
	
	
	/** methode qui ajoute un cycliste sur la case
	 * 
	 * @param cy cycliste
	 */
	public void setCycliste(Cycliste cy) {
		this.cycliste=cy;
	}
	
	
	/** methode qui retourne le cote de la case
	 * 
	 * @return cote de la case
	 */
	public boolean getDroite() {
		return this.droite;
	}
	
	
	/** methode qui retourne le cycliste sur la case 
	 * 
	 * @return cycliste
	 */
	public Cycliste getCycliste() {
		return cycliste;
	}


	public String  toString() {

        String s3 = "";
		Cycliste c = this.getCycliste();
		if (c == null){
			s3+= "   ";
            //s3+= " NN";
		}
		else if(c instanceof Sprinteur){
			s3+= " S"+c.getCouleur().substring(0,1);
		}
		else{
			s3+= " R"+c.getCouleur().substring(0,1);
		}
		s3 += " |";
		return s3;
	}

    public String  makeType() {
        		int i = this.type;
		String s ="";
		switch (i){
			case 0:
				s ="=";
                s = new String(new char[4]).replace("\0", s);
				break;
			case 1:
				s= "~";
                s = new String(new char[4]).replace("\0", s);
				break;
			case 2:
				s = "D";
                s = new String(new char[4]).replace("\0", s);
				break;
			case 3:
				s = "A";
                s = new String(new char[4]).replace("\0", s);
				break ;

		}
        s +="|";
        return  s;
    }

    public String makeRelief(){
	    String s2 ="";
		int i = this.relief;
		switch (i){
			case 0:
				s2 +="=";
                s2 = new String(new char[4]).replace("\0", s2);
				break;
			case 1:
				s2+= "\\";
                s2 = new String(new char[4]).replace("\0", s2);
				break;
			case 2:
				s2 += "/";
                s2 = new String(new char[4]).replace("\0", s2);
				break;
		}
        s2 +="|";
		return s2;
    }

}
