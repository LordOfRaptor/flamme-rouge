package CasePackage;
import CyclistePackage.Cycliste;

import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.io.Serializable;
import java.util.*;

/**
 * cette classe permet de creer un circuit a partir de fichier .txt
 * le circuit sera compose de case 
 * 
 * @author Tristan 
 * @version 1.0
 * 
 */
public class Circuit implements Serializable {
	

	/** attribut qui correspond au nom du circuit
	 * il permettra de differencier les differents circuits
	 * 
	 */
	public String nomCircuit;
	
	
	/** attribut qui correspond aux cases du circuit
	 * il y aura deux listes contenues dans une liste
	 * 		une pour les cases de droites (1)
	 * 		et une autres pour les cases de gauches (0)
	 * et chaque liste sera constitue de case du circuit 
	 * 
	 */
	public List<ArrayList<Case>> listeCase;
	
	
	/** attribut qui permet de savoir si un cyclite est sur la case ou pas 
	 * 
	 */
	public boolean estSur;
	
	
	/** constructeur de la classe circuit
	 *  permet de d instancier les deux ArrayList
	 *  
	 */
	public Circuit() {
		this.listeCase= new ArrayList<ArrayList<Case>>();
		this.listeCase.add(new ArrayList<Case>());
		this.listeCase.add(new ArrayList<Case>());
	}
	
	
	/** methode qui permet de creer un circuit 
	 * cette methode appel d autre methode qui permette de creer des lignes et des virages
	 * 
	 */
	public void setCircuit() {
		//circuit 1 classique
		this.nomCircuit="Classique";
		boolean b=false;
		for(int i=0;i<2;i++) {
			this.ligneDepart(i,b);
			this.petiteLigneDroite(i,b);
			this.ligneDroite(i,3,b); //b c d
			this.virage(i,1,b); // e
			this.ligneDroite(i,1,b); // f
			this.virage(i,5,b); // g h i j k
			this.ligneDroite(i,3,b); // l m n 	
			this.virage(i,6,b); // o p q r s
			this.petiteLigneDroite(i,b);
			this.ligneArrive(i,b);
			b=!b;
		}
	}
	
	
	/** methode qui creer un circuit avec du relief et des zones de plat
	 * mixte entre les tuiles (3 cases descente et 3 cases montee par exemple) 
	 * 
	 */
	public void setCircuitMix() {
		this.nomCircuit="CircuitR";
		boolean b=false;
		for(int i=0;i<2;i++) {
			this.ligneDepart(i,b);
			this.petiteLigneDroite(i,b);
			this.ligneDroite(i,1,b);
			this.virageDescente(i,b);
			this.virageDescente(i,b); 
			this.virage(i,1,b);
			this.petiteLigneDroite(i,b);
			this.petiteLigneDroite(i,b);
			for(int j=1;j<5;j++) {
				this.petiteLigneDroiteMontee(i,b);
			} 
			this.virageMontee(i,b);
			this.virageMontee(i,b); 
			for(int j=1;j<4;j++) {
				this.petiteLigneDroiteMontee(i,b);
			} 
			for(int j=1;j<8;j++) {
				this.petiteLigneDroiteDescente(i,b);
			} //B
			this.petiteLigneDroite(i,b);
			this.petiteLigneDroite(i,b);
			this.virage(i,2,b);
			this.virageMontee(i,b);
			this.virageMontee(i,b);
			this.petiteLigneDroite(i,b);
			this.petiteLigneDroite(i,b);
			this.petiteLigneDroite(i,b);
			for(int j=1;j<4;j++) {
				this.petiteLigneDroiteMontee(i,b);
			} 
			this.virageMontee(i,b);
			this.virageMontee(i,b);
			this.virage(i,1,b);
			for(int j=1;j<4;j++) {
				this.petiteLigneDroiteDescente(i,b);
			}
			this.petiteLigneDroite(i,b);
			this.petiteLigneDroite(i,b);
			this.petiteLigneDroite(i,b);
			this.virage(i,1,b);
			for(int j=1;j<7;j++) {
				this.petiteLigneDroiteMontee(i,b);
			} 
			this.virageDescente(i,b);
			this.virageDescente(i,b); 
			this.virage(i,1,b);
			b=!b;
			this.petiteLigneDroite(i,b);
			this.ligneArrive(i,b);
		}
	}
	
	
	/** methode qui creer un circuit avec du relief 
	 * 
	 */
	public void setCircuitRelief() {
		//circuit 2 relief 
		this.nomCircuit = "CircuitRelief";
		boolean b=false;
		for(int i=0;i<2;i++) {
			this.ligneDepart(i,b);
			this.petiteLigneDroite(i,b);
			for(int j=1;j<4;j++) {
				this.petiteLigneDroiteMontee(i,b);
			} // b*
			for(int j=1;j<4;j++) {
				this.petiteLigneDroiteDescente(i,b);
			} // b*
			this.ligneDroite(i,2,b); // c d
			this.virageMontee(i,b); // e*
			this.virageDescente(i,b); // e*
			this.ligneDroite(i,1,b); // f
			this.virage(i,5,b); // g h i j k
			for(int j=1;j<4;j++) {
				this.petiteLigneDroiteMontee(i,b);
			} // l m*
			for(int j=4;j<7;j++) {
				this.petiteLigneDroiteDescente(i,b);
			} // l m*
			this.ligneDroite(i,1,b); // n 	
			this.virage(i,6,b); // o p q r s
			this.petiteLigneDroite(i,b);
			this.ligneArrive(i,b);
			b=!b;
		}
	}
	
	
	/** methode qui permet de creer un circuit suivant les demandes de l utilisateur 
	 * 
	 */
	public void creerCircuit(){
		boolean arret=false;
		while(!arret) {
			Scanner sc = new Scanner(System.in);
			try {
				System.out.println("Creation de circuit : "+"\n"+"Entrez le nom de votre circuit");
				String nom=sc.next();
				System.out.println("tuile depart initialise"+"\n"+"voulez vous mettre un virage (v) ou une ligne droite (l) en montee (m), en descente (d) ou plat (p) ?");
				String s= sc.next();
				System.out.println("combien voulez-vous de tuiles ?");
				int nb = sc.nextInt();
				this.nomCircuit = nom;
				int nbl=9;
				int nbv=10;
				boolean b=false;
				for(int j=0;j<2;j++) {
					this.ligneDepart(j,b);
					this.petiteLigneDroite(j,b);
					b=!b;
				}
				while(nbl>0 && nbv>0) {

					switch(s) {
					case "vm":
						this.tuileVirageMontee(nb);
						nbv-=nb;
					break;
					case "vd":
						this.tuileVirageDescente(nb);
						nbv-=nb;
					break;
					case "vp":
						this.tuileVirage(nb);
						nbv-=nb;
					break;
					case "lm":
						this.tuileLigneDroiteMontee(nb);
						nbl-=nb;
					break;
					case "ld":
						this.tuileLigneDroiteDescente(nb);
						nbl-=nb;
					break;
					case "lp":
						this.tuileLigneDroite(nb);
						nbl-=nb;
					break;
					default: 
						System.out.println("Mauvaise entree veuillez recommencez");
						break;
					}
					System.out.println("Ligne droite restante : "+nbl+", nombre de virage restante : "+nbv);
					System.out.println("Voulez vous mettre un virage (v) ou une ligne droite (l) en montee (m), en descente (d) ou plat (p) ?");
					s= sc.next();
					System.out.println("combien voulez-vous de tuiles ?");
					nb = sc.nextInt();
				}
				b=false;
				for(int j=0;j<2;j++) {
					this.petiteLigneDroite(j,b);
					this.ligneArrive(j,b);
					b=!b;
				}
				System.out.println("Tuile arrive posee, circuit termine"+"\n"+"Affichage circuit");
				arret=true;
			}catch (InputMismatchException ime) {
				System.out.println("la touche tapee n est pas un entier");
			}
		}
	}
	
	
	/** methode qui creer des lignes 
	 * 
	 * @param j liste droite ou gauche
	 * @param nb nombre de tuile successive 
	 */
	public void ligneDroite(int j,int nb,boolean b) {
		for(int i=0;i<(6*nb);i++) {
			this.listeCase.get(j).add(new Case(0,0,b));
		}
	}
	
	
	/** methode qui creer des virage 
	 * 
	 * @param j j liste droite ou gauche
	 * @param nb nombre de tuile successive
	 */
	public void virage(int j,int nb, boolean b) {
		for(int i=0;i<(2*nb);i++) {
			this.listeCase.get(j).add(new Case(1,0,b));
		}
	}
	

	/** methode qui creer des lignes
	 *
	 * @param j liste droite ou gauche
	 */
	public void petiteLigneDroite(int j,boolean b) {
			this.listeCase.get(j).add(new Case(0,0,b));
	}
	
	
	/** methode qui creer des lignes en montee
	 *
	 * @param j liste droite ou gauche
	 */
	public void petiteLigneDroiteMontee(int j,boolean b) {
			this.listeCase.get(j).add(new Case(0,2,b));
	}
	
	
	/** methode qui creer des lignes en descente 
	 *
	 * @param j liste droite ou gauche
	 */
	public void petiteLigneDroiteDescente(int j,boolean b) {
			this.listeCase.get(j).add(new Case(0,1,b));
	}
	
	
	/** methode qui creer des virage en montee 
	 * 
	 * @param j j liste droite ou gauche
	 */
	public void virageMontee(int j, boolean b) {
		this.listeCase.get(j).add(new Case(1,2,b));
	}
	
	
	/** methode qui creer des virage en descente  
	 * 
	 * @param j j liste droite ou gauche
	 */
	public void virageDescente(int j, boolean b) {
		this.listeCase.get(j).add(new Case(1,1,b));
	}
	
	
	/** permet de creer des tuiles de virage en montee grace a des cases 
	 * 
	 * @param nb nombre de tuile a jouter 
	 */
	public void tuileVirageMontee(int nb) {
		boolean b=false;
		for(int j=0;j<2;j++) {
			for(int i=0;i<(2*nb);i++) {
				this.listeCase.get(j).add(new Case(1,2,b));
			}
			b=!b;
		}
	}
	
	
	/** permet de creer des tuiles de virage en descente grace a des cases 
	 * 
	 * @param nb nombre de tuile a ajouter
	 */
	public void tuileVirageDescente(int nb) {
		boolean b=false;
		for(int j=0;j<2;j++) {
			for(int i=0;i<(2*nb);i++) {
				this.listeCase.get(j).add(new Case(1,1,b));
			}
			b=!b;
		}
	}
	
	
	/** permet de creer des tuiles de virage grace a des cases 
	 * 
	 * @param nb nombre de tuile a ajouter 
	 */
	public void tuileVirage(int nb) {
		boolean b=false;
		for(int j=0;j<2;j++) {
			for(int i=0;i<(2*nb);i++) {
				this.listeCase.get(j).add(new Case(1,0,b));
			}
			b=!b;
		}
		
	}
	
	
	/** permet de creer des tuiles de lignes droite grace a des cases 
	 * 
	 * @param nb nombre de tuile ajouter
	 */
	public void tuileLigneDroite(int nb) {
		boolean b=false;
		for(int j=0;j<2;j++) {
			for(int i=0;i<(6*nb);i++) {
				this.listeCase.get(j).add(new Case(0,0,b));
			}
			b=!b;
		}
	}

	
	/** permet de creer des tuiles de lignes droite en montee grace a des cases 
	 * 
	 * @param nb nombre de tuile a ajouter
	 */
	public void tuileLigneDroiteMontee(int nb) {
		boolean b=false;
		for(int j=0;j<2;j++) {
			for(int i=0;i<(6*nb);i++) {
				this.listeCase.get(j).add(new Case(0,2,b));
			}
			b=!b;
		}
	}
	
	
	/** permet de creer des tuiles de lignes droite en descente grace a des cases 
	 * 
	 * @param nb nombre de tuile a ajouter 
	 */
	public void tuileLigneDroiteDescente(int nb) {
		boolean b=false;
		for(int j=0;j<2;j++) {
			for(int i=0;i<(6*nb);i++) {
				this.listeCase.get(j).add(new Case(0,1,b));
			}
			b=!b;
		}
	}
	
	
	/**
	 * methode qui creer des lignes
	 *
	 * @param j liste droite ou gauche
	 */
	public void ligneDepart(int j,boolean b) {
		for (int i = 0; i < 5; i++) {
			this.listeCase.get(j).add(new Case(2, 0, b));
		}
	}

	
	/**
	 * methode qui creer des lignes
	 *
	 * @param j liste droite ou gauche
	 */
	public void ligneArrive(int j,boolean b) {
		for (int i = 0; i < 5; i++) {
			this.listeCase.get(j).add(new Case(3, 0, b));
		}
	}
	
	
	/** methode qui permet de retourner l attribut estSur
	 * 
	 * @param i position premiere liste
	 * @param j	position de la case
	 * 
	 * @return la valeur de estSur
	 * 
	 */
	public boolean isEstsur(int i,int j) {
		this.estSur=false;
		if(listeCase.get(i).get(j).getCycliste()!=null) {
			this.estSur=true;
		}
		return estSur;
	}
	
	
	/**	methode qui permet de savoir ou se situe un cycliste
	 * 
	 * @param c objet de cycliste 
	 * @return la postion du cycliste sur le circuit
	 */
	public int getPosition(Cycliste c) {
		int i=0;
		if(c.isFileDroite()) {
			for(Case ca: listeCase.get(1)) {
				if(ca.getCycliste() != null && ca.getCycliste().equals(c)) {
					return i;
				}else {
					i++;
				}
			}
		}else {
			for(Case ca: listeCase.get(0)) {
				if(ca.getCycliste() != null && ca.getCycliste().equals(c)) {
					return i;
				}else {
					i++;
				}
			}
		}
		return -1;
	}
	

	/** methode toString qui permet d afficher la totalite du circuit
	 * 
	 */
	@Override
	public String toString() {
		String s = "|";
		String s2 ="";
		String s3 = "";
		for(int i = 0;i<2;i++){
			s2 ="";
			s3 = "";
			for(Case c: this.listeCase.get(i)){
				s2 += c.makeType();
				s3 += c.makeRelief();
				s += c.toString();
			}

			s+="\n|";
		}
		s = "|"+s2+"\n"+"|"+s3+"\n"+s+s3+"\n"+"|"+s2+"\n";
		return s;
	}

	
	/** retourne de nom de la case en fonction de sa position dans les 2 listes
	 *
     * @param i position premiere liste
     * @param j	position de la case
	 * @return nom de la case
	 * 
	 */
	public String getNomCase(int i, int j) {
		return this.listeCase.get(i).get(j).getNom();
	}
	
	
	/** methode qui ajoute un cycliste sur une case
	 * 
	 * @param cy cycliste qui va etre ajoute 
     * @param i position premiere liste
     * @param j	position de la case
	 */
	public void setCircuitCycliste(Cycliste cy,int i,int j) {
		this.listeCase.get(i).get(j).setCycliste(cy);
	}
	
	
	/** methode qui initialise des cicuits en fonction du fichier lu en parametre
	 * 
	 * @param nom nom du fichier a lire 
	 * @throws IOException
	 * @throws ClassNotFoundException
	 */
	public void lireCircuit(String nom) throws IOException, ClassNotFoundException {
		ObjectInputStream fich = new ObjectInputStream(new FileInputStream("DataCircuit\\"+nom+".txt"));
		Circuit cir=(Circuit) fich.readObject();
		System.out.println(cir);
		fich.close();
	}
	
	
	/** methode qui permet d ecrire un objet de type circuit dans un fichier 
	 * afin que celui ci soit sauvegarde et soit utilise uterieurement 
	 * 
	 * @param c objet de type circuit 
	 * @param nom nom du fichier qui va servir a ecrire l objet
	 * @throws IOException
	 */
	public static void saveCircuit(Circuit c,String nom) throws IOException {
		ObjectOutputStream des = new ObjectOutputStream(new FileOutputStream("DataCircuit\\"+nom+".txt",true));
		des.writeObject(c);
		System.out.println("fait");
		des.close();
	}
	
	
	public static void main(String args[]) throws ClassNotFoundException, IOException {
			Circuit c = new Circuit();
			c.creerCircuit();
			System.out.println(c.toString());
			saveCircuit(c,c.nomCircuit);	
	}
}
