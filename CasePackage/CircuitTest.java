package CasePackage;

import static org.junit.Assert.*;

import org.junit.Test;

import CyclistePackage.Cycliste;
import CyclistePackage.Sprinteur;

public class CircuitTest {

	@Test
	public void testsetCircuit() {
		Circuit c = new Circuit();
		c.setCircuit();
		assertEquals("premiere case ligne droite plate","Ligne droite plate",c.getNomCase(0,0));
		assertEquals("virage plat ","virage plat",c.getNomCase(0,26));
	}
	
	@Test
	public void testCote() {
		Circuit c = new Circuit();
		c.setCircuit();
		Cycliste cycli = new Sprinteur("Rouge",false);
		c.setCircuitCycliste(cycli,0,0);
		assertEquals("le cycliste doit etre sur la premiere case",true,c.isEstsur(0,0));
		assertEquals("le cycliste doit etre sur la premiere case de gauche",false,c.listeCase.get(0).get(0).getDroite());
	}

	
	
}
