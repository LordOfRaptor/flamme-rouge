package CyclistePackage;

import CartePackage.Carte;
import CartePackage.CarteFatigue;

import java.util.*;

/**
 * Classe permettant de creer un Cycliste
 * elle est declarer abstract car on ne doit pas
 * pouvoir creer d'objet de type Cycliste on doit
 * obligatoirement creer une CarteFatigue ou une CarteEnergie
 *
 * @author quent
 * @version 1.0
 */
public abstract class Cycliste {
    /**
     * ArrayList du deck d'un Cycliste
     */
    private ArrayList<Carte> deck = new ArrayList<>();
    /**
     * ArrayList de la defausse d'un Cycliste
     */
    private ArrayList<Carte> defausse = new ArrayList<>();
    /**
     * ArrayList de la main d'un Cycliste
     */
    private ArrayList<Carte> main = new ArrayList<>();

    /**
     * type du cycliste sprinteur ou Rouleur
     */
    private String type;
    /**
     * couleur d'un Cycliste
     */
    private String couleur;
    /**
     * Booleen pour savoir de quel cote est le cycliste
     */
    private boolean fileDroite;


    /**
     * Constructeur d'un cycliste qui initialise son type
     * sa couleur et le booleen de la file
     * @param s
     *       le type de cycliste
     * @param couleur
     *      la couleur du cycliste
     * @param droite
     *      le booleen de la ligne du cycliste
     */
    public Cycliste(String s,String couleur,boolean droite) {
        this.type = s;
        this.couleur = couleur;
        this.fileDroite = droite;
        this.remplirDeck();


    }

    /**
     * Retourne le type de cycliste
     * @return
     *      type du cycliste
     */
    public String getType() {
        return type;
    }

    /**
     * Retourne la couleur du cycliste
     * @return
     *        la couleur du cycliste
     */
    public String getCouleur() {
        return couleur;
    }

    /**
     * retourne dans quelle file il est
     * @return
     *      boolean vrai si file droite
     */
    public boolean isFileDroite() {
        return fileDroite;
    }

    /**
     * Change la file du joueur
     * @param file
     *          booleen de la file du cycliste
     */
    public void setFileDroite(boolean file){
        this.fileDroite = file;
    }

    /**
     * Remplit les deck des cycliste
     */
    public abstract void remplirDeck();


    /**
     * Renvoie le deck du cycliste
     * @return
     *      le deck du cycliste
     */
    public ArrayList<Carte> getDeck() {
        return deck;
    }

    /**
     * Renvoie la main d'un cycliste
     * @return
     *        la main d'un cycliste
     */
    public ArrayList<Carte> getMain() {
        return main;
    }

    /**
     * Renvoie la defausse d'un cycliste
     * @return
     *        la defausse d'un cycliste
     */
    public ArrayList<Carte> getDefausse() {
        return defausse;
    }

    /**
     * Ajoute une carte dans le deck
     * si c'est une carte Fatigue la carte est
     * ajouté puis melanger
     * @param c
     *      Carte a ajoute
     */
    public void addCarte(Carte c){
            if(c instanceof CarteFatigue){
                this.getDeck().add(c);
                Collections.shuffle(this.getDeck());
            }
            else {
                this.getDeck().add(c);
            }
    }


    /**
     * Ajoute 4 cartes dans la main du joueur
     * et les supprimes du deck si jamais il n'y
     * a pas assez de carte dans le deck il prend les
     * cartes restantes du deck et l'ajoutes a la main
     * et remplace le deck par la defausse et melange.
     * Ensuite il ajoute les carte manquantes dans la main
     */
    void makeMain(){
        int i = 0 ;
        if (this.getDeck().size() < 4){
            i = this.getDeck().size();
            this.getMain().addAll(deck);
            this.getDeck().clear();
            this.defausseToDeck();
        }
        if (this.getDeck().size()+this.getMain().size() < 4){
            this.getMain().addAll(deck);
            this.getDeck().clear();
        }
        else {
            for (int j = i; j < 4; j++) {
                int random = (int) (Math.random() * this.getDeck().size());
                this.getMain().add(this.getDeck().get(random));
                this.getDeck().remove(random);
            }
        }
    }

    /**
     * Demande a l'utilisateur de choisir
     * une carte dans sa main il la renvoie
     * a l'utilisateur puis la supprimes du jeu
     * ensuite les cartes restabntes sont remises
     * dans la defausse
     * @return
     *      la carte choisie
     */
    public Carte choixCarte(){
        this.makeMain();
        Scanner sc = new Scanner(System.in);
        for(Carte c :this.getMain()){
            System.out.println(c.toString());
        }
        int val = 0;
        while(val < 1 || val > this.getMain().size()) {
            try {
                System.out.println("Rentrer la position de la carte choisie");
                val = sc.nextInt();
            } catch (InputMismatchException e){                     //Exception si jamais le scanner ne reçois pas un entier
                System.out.println("Valeur erronée : " + sc.next());   //Reinitialise le scanner pour rentrer une bonne valeur
            }
        }
        Carte c2 = this.getMain().get(val-1);
        this.getMain().remove(val-1);
        this.addDefausse();
        return c2;
    }

    /**
     * Transfert les cartes de la main
     * a la defausse et vide la main
     */
    void addDefausse(){
        this.getDefausse().addAll(this.getMain());
        this.getMain().clear();
    }

    /**
     * Remet les cartes de la defausse
     * dans le deck et melange
     */
    void defausseToDeck(){
        this.getDeck().addAll(this.getDefausse());
        this.getDefausse().clear();
        Collections.shuffle(this.getDeck());
    }

    /**
     * renvoie la file du cycliste sous forme de String
     * @return
     *      file du cycliste
     */
    private String ligne(){
        if (this.isFileDroite()){
            return "droite";
        }
        else{
            return "gauche";
        }
    }

    /**
     * Verifie une egalite entre 2 cycliste
     * @param obj
     *      objet de type cycliste
     * @return
     *      Retourne true si 2 cycliste sont egaux
     */
    @Override
    public boolean equals(Object obj) {
        Cycliste c = (Cycliste) obj;
       if (this.getType().equals(c.getType()) && this.getCouleur().equals(c.getCouleur())){
            return true;
       }
        return false;
    }

    /**
     * Renvoie le type du cycliste son équipe et sa file
     * @return
     *      Une phrase d'information sur le cycliste
     */
    @Override
    public String toString() {
        return "Je suis un " + this.type + " de l'equipe " + this.couleur + " et je suis sur la file de " + this.ligne();
    }
}
