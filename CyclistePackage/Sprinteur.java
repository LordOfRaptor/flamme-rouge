package CyclistePackage;

import CartePackage.CarteEnergie;

import java.util.Collections;

/**
 * Class du cycliste de type Sprinteur
 */
public class Sprinteur extends Cycliste {

    /**
     * Constructeur d'un cycliste qui de type
     * sprinteur initialise son type
     * sa couleur et le booleen de la file

     * @param couleur
     *      la couleur du cycliste
     * @param droite
     *      le booleen de la ligne du cycliste
     */
    public Sprinteur(String couleur,boolean droite){
        super("Sprinteur",couleur,droite);
    }

    /**
     * Remplit le deck du sprinteur et le melange
     */
    @Override
    public void remplirDeck() {
        for (int i = 0; i < 3; i++) {
            this.getDeck().add(new CarteEnergie(2));
            this.getDeck().add(new CarteEnergie(3));
            this.getDeck().add(new CarteEnergie(4));
            this.getDeck().add(new CarteEnergie(5));
            this.getDeck().add(new CarteEnergie(9));
        }
        Collections.shuffle(this.getDeck());
    }
}
