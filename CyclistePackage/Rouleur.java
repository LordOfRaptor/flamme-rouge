package CyclistePackage;

import CartePackage.CarteEnergie;
import java.util.Collections;

public class Rouleur extends Cycliste {

    /**
     * Constructeur d'un cycliste qui de type
     * rouleur initialise son type
     * sa couleur et le booleen de la file
     * @param couleur
     *      la couleur du cycliste
     * @param droite
     *      le booleen de la ligne du cycliste
     */
    public Rouleur(String couleur,boolean droite){
        super("Rouleur",couleur,droite);
    }

    /**
     * Remplit le deck du rouleur et le melange
     */
    @Override
    public void remplirDeck(){
        for (int i =0;i<3;i++){
            this.getDeck().add(new CarteEnergie(3));
            this.getDeck().add(new CarteEnergie(4));
            this.getDeck().add(new CarteEnergie(5));
            this.getDeck().add(new CarteEnergie(6));
            this.getDeck().add(new CarteEnergie(7));
        }
        Collections.shuffle(this.getDeck());
    }
}
