package CyclistePackage;

import CartePackage.Carte;
import CartePackage.CarteEnergie;
import CartePackage.CarteFatigue;
import org.junit.Test;

import java.util.InputMismatchException;
import java.util.Scanner;

import static org.junit.Assert.*;

public class CyclisteTest {

    /**
     * Test si le type est bien renvoyer
     */
    @Test
    public void getType() {
        Cycliste c = new Sprinteur("Rouge",false);
        assertEquals("N'est pas un Sprinteur","Sprinteur",c.getType());
    }

    /**
     * Test le couleur est bonne
     */
    @Test
    public void getCouleur() {
        Cycliste c = new Sprinteur("Rouge",false);
        assertEquals("N'est pas rouge","Rouge",c.getCouleur());
    }

    /**
     * Test si la file est bien la bonne
     */
    @Test
    public void isFileDroite() {
        Cycliste c = new Sprinteur("Rouge",false);
        assertEquals("N'est pas faux",false,c.isFileDroite());
    }

    /**
     * Test si la file est bien changer
     */
    @Test
    public void setFileDroite() {
        Cycliste c = new Sprinteur("Rouge",false);
        c.setFileDroite(true);
        assertEquals("devrait etre vrai",true,c.isFileDroite());
    }

    /**
     * Test si le deck est bien remplie
     * mais aussi si il contient une carte spécifique
     */
    @Test
    public void remplirDeck() {
        Cycliste c = new Sprinteur("Rouge",false);
        assertEquals("Pas la bonne taille",15,c.getDeck().size());
        assertEquals("Pas la bonne taille",true,c.getDeck().contains(new CarteEnergie(9)));
    }

    /**
     * Test si l'ajout d'une carte fonctionne en ragardant
     * si tailel augmente
     */
    @Test
    public void addCarte() {
        Cycliste c = new Sprinteur("Rouge",false);
        c.addCarte(new CarteFatigue(2));
        assertEquals("Pas la bonne taille",16,c.getDeck().size());
    }

    /**
     * Test si la main est initialiser et si la
     * deck est bien vider des cartes
     */
    @Test
    public void makeMain(){
        Cycliste c = new Sprinteur("Rouge",false);
        c.makeMain();
        assertEquals("Pas la bonne taille",4,c.getMain().size());
        assertEquals("Pas la bonne taille",11,c.getDeck().size());
    }

    /**
     * Test pour savoir si quand le deck est vide il
     * vient dans la defausse
     */
    @Test
    public void makeMainVide(){
        Cycliste c = new Sprinteur("Rouge",false);
        c.getDefausse().addAll(c.getDeck());
        c.getDeck().clear();
        assertEquals("Pas la bonne taille",15,c.getDefausse().size());
        assertEquals("Pas la bonne taille",0,c.getDeck().size());
        c.makeMain();
        assertEquals("Pas la bonne taille",0,c.getDefausse().size());
        assertEquals("Pas la bonne taille",11,c.getDeck().size());
        assertEquals("Pas la bonne taille",4,c.getMain().size());
    }

    /**
     * Test pour savoir si il ajoute bien la dernière carte
     * du deck avant d'ajouter des cartes de la defausse
     */
    @Test
    public void makeMainDeckPresqueVide(){
        Cycliste c = new Sprinteur("Rouge",false);
        while(c.getDeck().remove(new CarteEnergie(3))){}
        assertEquals("Pas la bonne taille",12,c.getDeck().size());
        c.getDefausse().addAll(c.getDeck());
        c.getDeck().clear();
        assertNotEquals(true,c.getDeck().contains(new CarteFatigue(3)));
        c.getDeck().add(new CarteFatigue(3));
        assertEquals("Pas la bonne taille",1,c.getDeck().size());
        assertEquals(true,c.getDeck().contains(new CarteFatigue(3)));
        c.makeMain();
        assertEquals(true,c.getMain().contains(new CarteFatigue(3)));
        assertEquals("Pas la bonne taille",0,c.getDefausse().size());
        assertEquals("Pas la bonne taille",9,c.getDeck().size());
        assertEquals("Pas la bonne taille",4,c.getMain().size());
    }

    /**
     * Test si la methode choixcarte fonctionne
     * Ne peut etre testé car presence d'un scanner
     */
//    @Test
//    public void choixCarte(){
//        Cycliste c = new Sprinteur("Rouge",false);
//        c.main();
//        c.choixCarte();
//        assertEquals("Pas la bonne taille",0,c.getMain().size());
//        assertEquals("Pas la bonne taille",11,c.getDeck().size());
//        assertEquals("Pas la bonne taille",3,c.getDefausse().size());
//    }



    /**
     * Test si la methode transfert bien la
     * main a la defausse
     */
    @Test
    public void addDefausse(){
        Cycliste c = new Sprinteur("Rouge",false);
        c.makeMain();
        assertEquals("Pas la bonne taille",4,c.getMain().size());
        assertEquals("Pas la bonne taille",11,c.getDeck().size());
        c.addDefausse();
        assertEquals("Pas la bonne taille",0,c.getMain().size());
        assertEquals("Pas la bonne taille",11,c.getDeck().size());
        assertEquals("Pas la bonne taille",4,c.getDefausse().size());
    }

    /**
     * Test si la defausse va bien dans le deck
     */
    @Test
    public void defausseToDeck(){
        Cycliste c = new Sprinteur("Rouge",false);
        c.makeMain();
        assertEquals("Pas la bonne taille",4,c.getMain().size());
        assertEquals("Pas la bonne taille",11,c.getDeck().size());
        c.addDefausse();
        c.defausseToDeck();
        assertEquals("Pas la bonne taille",15,c.getDeck().size());
    }

    /**
     * Test de la methode toString
     */
    @Test
    public void toSring(){
        Cycliste c = new Sprinteur("Rouge",false);
        Cycliste c1 = new Sprinteur("Bleu",true);
        assertEquals("Je suis un Sprinteur de l'equipe Rouge et je suis sur la file de gauche",c.toString());
        assertEquals("Je suis un Rouleur de l'equipe Bleu et je suis sur la file de droite",c1.toString());
    }
}