package CyclistePackage;

public class CyclisteException extends Exception {

    public CyclisteException(){
        super();
    }

    public CyclisteException(String s){
        super(s);
    }
}
