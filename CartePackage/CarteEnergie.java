package CartePackage;

/**
 * Class de la carte du deck energie
 */
public class CarteEnergie extends Carte{

    /**
     *Creation d'une carte energie
     * @param val
     *         valeur de la carte
     */
    public CarteEnergie(int val) {
        super(val);
    }


    /**
     * Verifie une egalite entre 2 cartes
     * @param obj
     *      Objet de type carte energie
     * @return
     *      true si la carte est egale a une autre
     */
    @Override
    public boolean equals(Object obj) {
        if (obj instanceof CarteEnergie ) {
            CarteEnergie c = (CarteEnergie) obj;
            if (c.getValeur() == this.getValeur()) {
                return true;
            } else {
                return false;
            }
        }
        else{
            return false;
        }
    }

    /**
     * renvoie le type de carte et sa valeur
     * @return
     *      un string du type de carte et sa valeur
     */
    @Override
    public String toString() {
        return "Carte Energie :" + this.getValeur();
    }
}
