package CartePackage;

/**
 * Class de la carte du deck Fatigue
 */
public class CarteFatigue extends Carte{

    /**
     *Creation d'une carte fatigue
     * @param val
     *         valeur de la carte
     */
    public CarteFatigue(int val) {
        super(val);
    }

    /**
     * Verifie une egalite entre 2 cartes
     * @param obj
     *      Objet de type carte fatigue
     * @return
     *      true si la carte est egale a une autre
     */
    @Override
    public boolean equals(Object obj) {
        if (obj instanceof CarteFatigue ) {
            CarteFatigue c = (CarteFatigue) obj;
            if (c.getValeur() == this.getValeur()) {
                return true;
            } else {
                return false;
            }
        }
        else{
            return false;
        }
    }

    /**
     * renvoie le type de carte et sa valeur
     * @return
     *      un string du type de carte et sa valeur
     */
    @Override
    public String toString() {
        return "Carte Fatigue :" + this.getValeur();
    }
}
