package CartePackage;

/**
 * Classe permettant de creer une carte
 * elle est declarer abstract car on ne doit pas
 * pouvoir creer d'objet de type Carte on doit
 * obligatoirement creer une CarteFatigue ou une CarteEnergie
 *
 * @author quent
 * @version 1.0
 */

public abstract class Carte {
    /**
     * De combien la carte permet de se deplacer
     */
    private int valeur;

    /**
     * Création d'une carte
     * @param val
     * Valeur de la carte
     */

    Carte(int val){
        this.valeur = val;
    }

    /**
     * Retourne la valeur de la carte
     * @return
     *      valeur de la carte
     */
    public int getValeur() {
        return valeur;
    }
}
