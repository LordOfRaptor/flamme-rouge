package JeuPackage;

import CartePackage.Carte;
import CartePackage.CarteFatigue;
import CasePackage.Case;
import CasePackage.Circuit;
import CyclistePackage.Cycliste;
import CyclistePackage.Rouleur;
import CyclistePackage.Sprinteur;


import java.util.*;

public class Jeu {
    private ArrayList<Joueur> listeJoueurs;
    private Circuit circuit;
    private int nbJoueurs;
    private static ArrayList<CarteFatigue> deckCarteFatigueRouleur = new ArrayList<>();
    private static ArrayList<CarteFatigue> deckCarteFatigueSprinteur = new ArrayList<>();
    private static ArrayList<String> listeCouleur = new ArrayList<>();

    public Jeu(int nb){
        this.circuit = new Circuit();
        this.nbJoueurs = nb;
        this.listeJoueurs = new ArrayList<>();
        Scanner sc = new Scanner(System.in);
        for (int i = 0;i<this.nbJoueurs;i++){
            System.out.println("Rentrez un nom de joueur");
            String nom = sc.next();
            while (this.joueurExistant(nom)){
                System.out.println("Nom deja pris, entrez un nouveau nom");
                nom = sc.next();
            }
            this.listeJoueurs.add(new Joueur(nom,listeCouleur.get(i),new Rouleur(listeCouleur.get(i),false)
                    ,new Sprinteur(listeCouleur.get(i),false)));
        }
    }

    public static void remplir(){
        for(int i = 0;i<60;i++){
            deckCarteFatigueRouleur.add(new CarteFatigue(2));
            deckCarteFatigueSprinteur.add(new CarteFatigue(2));
        }
    }
    public void circuitNormal(){
        this.circuit.setCircuit();
    }

    public void circuitRelief(){
        this.circuit.setCircuitRelief();
    }

    private static int initJoueur(){
        Scanner sc = new Scanner(System.in);
        int i = 0;
        while(i<2 || i>4){
            try {
                System.out.println("Rentrez le nombre de joueurs");
                i = sc.nextInt();
            }
            catch (InputMismatchException e){
                System.out.println("Pas un nombre : " + sc.next());
            }
        }
        return i;

    }

    private boolean joueurExistant(String nom){
        for(Joueur j : this.listeJoueurs){
            if(j.nomJoueur.equals(nom)){
                return true;
            }
        }
        return false;
    }

    private void avanceligneDroite(Cycliste cy){
        int file;
        if(cy.isFileDroite()){
            file = 1;
        }
        else {
            file = 0;
        }
        int pos = this.circuit.getPosition(cy);
        this.circuit.setCircuitCycliste(null,file,pos);
        this.circuit.setCircuitCycliste(cy,file,pos+1);
    }

    private void applicationAspiration(){
        ArrayList<Cycliste> listeCycliste= this.trie();
        Collections.reverse(listeCycliste);
        boolean fini = false;
        while (!(fini)){
            fini = true;
            for(int i = 0;i<listeCycliste.size();i++){
                int posCyc = this.circuit.getPosition(listeCycliste.get(i));
                if(this.circuit.listeCase.get(1).get(posCyc+2).getCycliste() != null && this.circuit.listeCase.get(1).get(posCyc+1).getCycliste() == null
                        && this.circuit.listeCase.get(1).get(posCyc+2).relief !=2 && this.circuit.listeCase.get(1).get(posCyc).relief !=2){
                    this.avanceligneDroite(listeCycliste.get(i));
                    fini = false;
                }
            }
        }
        System.out.println();
    }

    public void applicationFatigue(){
        ArrayList<Cycliste> lisCyc = this.trie();
        for (Cycliste c : lisCyc){
            int pos = this.circuit.getPosition(c);
            if (this.circuit.listeCase.get(1).get(pos+1).getCycliste() == null){
                if (c instanceof Sprinteur){
                    c.getDefausse().add(deckCarteFatigueSprinteur.get(0));
                    deckCarteFatigueSprinteur.remove(0);
                }
                if (c instanceof Rouleur){
                    c.getDefausse().add(deckCarteFatigueRouleur.get(0));
                    deckCarteFatigueRouleur.remove(0);
                }
                System.out.println(c);
            }
        }
    }

    private static void initCouleur(){
       listeCouleur.add("Rouge");
        listeCouleur.add("Bleu");
        listeCouleur.add("Vert");
        listeCouleur.add("Jaune");
    }



    private void Tour(){
        Scanner sc = new Scanner(System.in);
        ArrayList<Cycliste> cycFinal  = new ArrayList<>();
        ArrayList<Cycliste> tmp = new ArrayList<>();
        ArrayList<Carte> carteListe = new ArrayList<>();
        System.out.println(this.circuit.toString());
        for(int i = 0; i<this.nbJoueurs;i++){
            System.out.println(this.listeJoueurs.get(i).nomJoueur+"("+this.listeJoueurs.get(i).couleur+")" +" choisissez qui du rouleur(R) ou du Sprinteur(S) " +
                    "vous voulez faire avancez");
            String s = sc.next();
            s = s.toUpperCase();
            while (!(s.equals("S") || s.equals("R"))){
                System.out.println("S'il vous plait veuillez rentrez juste R ou S (minuscule ou majuscule)");
                s = sc.next();
                s = s.toUpperCase();
            }
            switch (s){
                case "S":
                    tmp.add(this.listeJoueurs.get(i).rouleur);
                    carteListe.add(this.listeJoueurs.get(i).sprinteur.choixCarte());
                    cycFinal.add(this.listeJoueurs.get(i).sprinteur);
                    break;
                case "R":
                    tmp.add(this.listeJoueurs.get(i).sprinteur);
                    carteListe.add(this.listeJoueurs.get(i).rouleur.choixCarte());
                    cycFinal.add(this.listeJoueurs.get(i).rouleur);
                    break;
                }

            }
        for(int j = 0; j<this.nbJoueurs;j++){
            System.out.println(this.listeJoueurs.get(j).nomJoueur + " vous allez joué votre 2ème cycliste ");
            carteListe.add(tmp.get(j).choixCarte());
            cycFinal.add(tmp.get(j));

        }
        tmp = this.trie();
        for(int k = 0;k<tmp.size();k++){
            int pos = cycFinal.indexOf(tmp.get(k));
            int val = carteListe.get(pos).getValeur();
            Cycliste cy = cycFinal.get(pos);
            val = this.appliqueDesc(cy,val);
            val = this.appliqueMonte(cy,val);
            this.faireAvancer(cy,val);
        }
    }

    public int appliqueMonte(Cycliste cy,int val){
        int pos = this.circuit.getPosition(cy);
        if(val <=5){
            return val;
        }
        if (this.circuit.listeCase.get(1).get(pos).relief == 2 && val >= 5){
            return 5;
        }
        boolean montee = false;
        for(int i = pos;i<=(pos+val);i++){
            if(this.circuit.listeCase.get(1).get(i).relief == 2){
                montee = true;
                if(i-pos>5){
                    return i-pos-1;
                }
            }
        }
        if(montee){
            return 5;
        }
        else{
            return val;
        }
    }


    public int appliqueDesc(Cycliste cy,int val){
        int pos = this.circuit.getPosition(cy);
        if (this.circuit.listeCase.get(1).get(pos).relief == 1 && val <= 5){
            return 5;
        }
        else{
            return val;
        }
    }

    private ArrayList<Cycliste> trie(){
        ArrayList<Cycliste> listecyc= new ArrayList<>();
        for(Joueur j : this.listeJoueurs){
            listecyc.add(j.rouleur);
            listecyc.add(j.sprinteur);
        }
        Comparator cmp = new Comparator() {
            @Override
            public int compare(Object o1, Object o2) {
                Cycliste c1 = (Cycliste) o1;
                Cycliste c2 = (Cycliste) o2;
                if(c1.isFileDroite()) {
                    return circuit.getPosition(c2) - circuit.getPosition(c1)-1;
                }
                else {
                    return circuit.getPosition(c2) - circuit.getPosition(c1)+1;
                }
            }
        };
        Collections.sort(listecyc,cmp);
        return listecyc;
    }

    public Cycliste premierCyc(){
        ArrayList<Cycliste> cycListe = this.trie();
        return cycListe.get(0);
    }

    public boolean fini(){
        int index = this.circuit.getPosition(this.premierCyc());
        return (this.circuit.listeCase.get(1).get(index).type == 3);

    }
    private void faireAvancer(Cycliste cy,int val){
        int file;
        if(cy.isFileDroite()){
            file = 1;
        }
        else {
            file = 0;
        }
        int pos = this.circuit.getPosition(cy);
        this.circuit.setCircuitCycliste(null,file,pos);
        int i = pos+val;
        if (i>this.circuit.listeCase.get(0).size()){
            i = this.circuit.listeCase.get(0).size() -1;
        }
        int j =1;
        while(i>=pos && this.circuit.listeCase.get(j).get(i).getCycliste()!=null){
            j=1;
            while (j>0 && this.circuit.listeCase.get(j).get(i).getCycliste()!=null){
               j--;
            }
            if(j!= -1 && this.circuit.listeCase.get(j).get(i).getCycliste()!=null) {
                i--;
            }
        }
        this.circuit.setCircuitCycliste(cy,j,i);
        boolean droite;
        if(j == 1){
            droite = true;
        }
        else{
          droite = false;
        }
        cy.setFileDroite(droite);
    }

    private void afficherDepart(){
        Cycliste c;
        for(int i= 0;i<2;i++){
            for(int j = 0;j<5;j++) {
                c= this.circuit.listeCase.get(i).get(j).getCycliste();
                if(c == null){
                    System.out.print("[ 0"+(i*5+j)+" ]");
                }
                else{
                    System.out.print("[ "+c.getType().substring(0,1)+c.getCouleur().substring(0,1)+" ]");
                }
            }
            System.out.println("");
        }
    }


    public void placementJoueur(){
        Scanner sc = new Scanner(System.in);
        int place;
        boolean pasValide;
        for (Joueur j : this.listeJoueurs){
            //TODO verif non null;
            this.afficherDepart();
            pasValide =true;
            while(pasValide){
                try {
                    System.out.println("Rentrer la place pour le sprinteur");
                    place = sc.nextInt();
                    while (place < 0 || place > 9 || this.circuit.listeCase.get(place / 5).get(place % 5).getCycliste() != null) {
                        System.out.println("Place déjà prise ou en dehors du plateau\nRentrez une nouvelle valeur");
                        place = sc.nextInt();
                    }
                    j.sprinteur = new Sprinteur(j.couleur,this.circuit.listeCase.get(place/5).get(0).getDroite());
                    this.circuit.setCircuitCycliste(j.sprinteur,place/5,place%5);
                    pasValide = false;
                } catch (InputMismatchException e) {
                    System.out.println("Pas un chiffre : " + sc.next());
                }
            }
            pasValide = true;
            this.afficherDepart();
            while(pasValide){
                try {
                    System.out.println("Rentrer la place pour le rouleur");
                    place = sc.nextInt();
                    while (place <0 || place > 9 || this.circuit.listeCase.get(place/5).get(place%5).getCycliste() != null) {
                        System.out.println("Place déjà prise ou en dehors du plateau\nRentrez une nouvelle valeur");
                        place = sc.nextInt();
                    }
                    j.rouleur = new Rouleur(j.couleur,this.circuit.listeCase.get(place/5).get(0).getDroite());
                    this.circuit.setCircuitCycliste(j.rouleur,place/5,place%5);
                    pasValide = false;
                } catch (InputMismatchException e) {
                    System.out.println("Pas un chiffre : " + sc.next());
                }
            }
        }
    }

    public static void main(String[] args) {
        int nb = initJoueur();
        initCouleur();
        remplir();
        Jeu j = new Jeu(nb);
        j.circuitRelief();
        j.placementJoueur();
        while (!(j.fini())){
            j.Tour();
            j.applicationAspiration();
            j.applicationFatigue();
        }
        System.out.println("Le premier est : " + j.premierCyc());


    }
}
