package JeuPackage;
import CyclistePackage.*;
/**
 * cette classe permet de creer un Joueur avec son nom et le nom de son equipe
 * 
 * @author Tristan 
 * @version 1.0
 * 
 */
public class Joueur {
	
	
	/** attribut correspondant au nom du joueur
	 * 
	 */
	public String nomJoueur;
	
	
	/** attribut correspondant au nom de l equipe
	 * 
	 */
	public String couleur;
	
	public Rouleur rouleur;
	
	public Sprinteur sprinteur;
	
	/** constructeur de Joueur 
	 * permet d initialiser le nom du joueur et le nom de son equipe
	 * permet egalement d initialiser ses joueurs 
	 * 
	 * @param nj nom joueur
	 * @param c couleur equipe
	 * @param r rouleur du joueur
	 * @param s sprinteur du joueur 
	 * 
	 */
	public Joueur(String nj,String c,Rouleur r, Sprinteur s) {
		this.nomJoueur=nj;
		this.couleur=c;
		this.rouleur=r;
		this.sprinteur=s;
	}
	
}
